import {
  BrowserRouter as Router,
  Route,
  Routes,
  RouteObject,
} from 'react-router-dom';
import routes from 'routes/index';

const renderRouters = (routers?: RouteObject[]) =>
  routers &&
  routers.map(({ path, element, children }, index: number) => (
    <Route key={`${path!}${index}`} path={path} element={element}>
      {children && renderRouters(children)}
    </Route>
  ));

function App(): JSX.Element {
  return (
    <Router>
      <Routes>{renderRouters(routes)}</Routes>
    </Router>
  );
}

export default App;
