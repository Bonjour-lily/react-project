import { createRoot } from 'react-dom/client';
import { StrictMode } from 'react';
import App from './app';

// const App = () => {
//   return <div>hello my-cra</div>;
// };

export default App;

createRoot(document.getElementById("root")!).render(
  <StrictMode>
    <App />
  </StrictMode>
);