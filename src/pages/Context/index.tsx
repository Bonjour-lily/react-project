import { PropsWithChildren, createContext } from 'react';

export const HomeContext = createContext<any>({});

function HomeProvider({ children }: PropsWithChildren): JSX.Element {
  return (
    <HomeContext.Provider value={{ prop1: 'hi' }}>
      {children}
    </HomeContext.Provider>
  );
}
export default HomeProvider;
