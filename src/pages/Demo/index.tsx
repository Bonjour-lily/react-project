import styles from './demo.scss';

function Demo(): JSX.Element {
  return (
    <div className={styles['demo-container']}>
      <div>this is Demo</div>
    </div>
  );
}

export default Demo;
