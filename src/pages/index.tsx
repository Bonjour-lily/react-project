import HomeProvider from './Context';
import Demo from './Demo';

function HomePage(): JSX.Element {
  return (
    <HomeProvider>
      <div>Home</div>
      <Demo />
    </HomeProvider>
  );
}

export default HomePage;
