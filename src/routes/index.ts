import type { RouteObject } from 'react-router';

import { load } from 'utils/index';

const routes: RouteObject[] = [
  {
    path: '/',
    element: load('index.tsx'),
    children: [
      {
        path: '/demo/page',
        element: load('Demo/index.tsx'),
      },
    ],
  },
];

export default routes;
