const { resolve } = require('path');
const WebpackBar = require('webpackbar');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { EsbuildPlugin } = require('esbuild-loader');
const package_ = require('./package.json');

const rootDir = resolve(__dirname, '../react-project');
console.log('rootDir:', rootDir);
const SERVICE = process.env.SERVICE || '';

const { MODE = 'development' } = process.env;

const isDev = MODE === 'development';

module.exports = {
  entry: ['./src/index.tsx'],
  mode: MODE,
  devtool: isDev ? 'source-map' : false,
  target: 'web',
  output: {
    publicPath: '/',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.css', '.scss'],
    alias: {
      components: resolve('src/components'),
      modules: resolve('src/modules'),
      constant: resolve('src/constant'),
      hooks: resolve('src/hooks'),
      layouts: resolve('src/layouts'),
      styles: resolve('src/styles'),
      services: resolve('src/services'),
      types: resolve('src/types'),
      assets: resolve('src/assets'),
      utils: resolve('src/utils'),
      locales: resolve('src/locales'),
      routes: resolve('src/routes'),
      widgets: resolve('src/widgets'),
      context: resolve('src/context'),
      pages: resolve('src/pages'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'esbuild-loader',
        exclude: /node_modules/,
        options: {
          loader: 'tsx',
          target: 'es2015',
        },
      },
      {
        test: /\/((src\/(scss|components|widgets))|(node_modules))\/(.*)\.(sa|sc|c)ss$/,
        use: [
          isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDev,
              // sassOptions: {
              //   includePaths: [resolve('src/scss')],
              // },
            },
          },
        ],
      },
      {
        test: /\/(src)\/(modules|pages)\/(.*)\.scss$/,
        use: [
          isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              // mode: 'local',
              sourceMap: isDev,
              modules: {
                localIdentName: isDev
                  ? '[path][name]_[local]'
                  : '[hash:base64]',
                // context: resolve('src'),
              },
            },
          },
          !isDev && {
            loader: 'postcss-loader',
            options: {
              sourceMap: isDev,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDev,
            },
          },
        ].filter(Boolean),
      },
      {
        generator: {
          filename: 'static/images/[name].[hash][ext]',
        },
        test: /\.(jpe?g|png|gif|svg)$/,
        type: 'asset/resource',
      },
      {
        generator: {
          filename: 'static/font/[name].[hash][ext]',
        },
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
      },
    ],
  },
  devServer: {
    compress: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    historyApiFallback: true,
    host: '0.0.0.0',
    hot: true,
    open: true,
    port: 8081,
    static: [resolve(rootDir, 'dist')],
  },
  plugins: [
    new WebpackBar(),
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      ENV: JSON.stringify(process.env.NODE_ENV),
      PRODUCTION: JSON.stringify(process.env.NODE_ENV === 'production'),
      PROJECT: JSON.stringify(package_.name),
      SERVICE: JSON.stringify(SERVICE),
      VERSION: JSON.stringify(package_.version),
    }),
    !isDev &&
      new MiniCssExtractPlugin({
        filename: 'static/css/[name].[contenthash].css',
      }),
    new HtmlWebpackPlugin({
      template: resolve(rootDir, 'public', 'index.html'),
    }),
  ].filter(Boolean),
  optimization: {
    minimizer: [
      new EsbuildPlugin({
        target: 'es2015',
        css: true,
      }),
    ],
    splitChunks: {
      chunks: 'all',
      minChunks: 1,
      cacheGroups: {
        vendors: {
          chunks: 'initial',
          minChunks: 1,
          name: 'vendor',
          priority: 1,
          test: /[\\/]node_modules[\\/].*\.js$/,
        },
        'async-vendors': {
          chunks: 'async',
          minChunks: 1,
          name: 'async-vendors',
          priority: -20,
          test: /[\\/]node_modules[\\/].*\.js$/,
        },
      },
    },
    chunkIds: 'named',
    moduleIds: 'named',
    concatenateModules: true,
    emitOnErrors: false,
    mergeDuplicateChunks: true,
    removeEmptyChunks: true,
    removeAvailableModules: true,
    providedExports: true,
    usedExports: true,
  },
  performance: {
    hints: false,
  },
};
